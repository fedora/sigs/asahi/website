#!/bin/bash
set -e

VERSION="Fedora Linux 41"
TARGET_VERSION="Fedora Linux 41"
PACKAGE_BASE=""

curl -s https://fedora-asahi-remix.org/installer_data.json > /tmp/far.json

BUILD=$(jq -r < /tmp/far.json \
    ".os_list[].name | select(. | test(\"$VERSION\")) | sub(\".*\\\\((?<i>.*)\\\\).*\"; \"\\(.i)\")" \
    | sort -r | head -1)

echo "Using build: $BUILD"

jq < installer_data_stable.json > /tmp/new.json --indent 2 --slurpfile far /tmp/far.json \
    ".os_list |=
        [
            \$far[0].os_list[]
            | select(.name | test(\"$VERSION.*$BUILD\"))
            | (.name |= sub(\"$VERSION (?<i>.*) \\\\(.*\\\\)\"; \"$TARGET_VERSION \\(.i)\"))
            | (.package |= \"$PACKAGE_BASE\" + .)
        ] +
        [
            .[] | select(.name | test(\"Fedora\") == false)
        ]
    "

diff -u installer_data_stable.json /tmp/new.json && echo "No change" && exit 0

if [ "$1" == "-u" ] ; then
    git diff-index --quiet --cached HEAD -- || { echo "There are uncommitted changes, not updating" ; exit 1 ; }
    mv /tmp/new.json installer_data_stable.json
    git add installer_data_stable.json
    git commit -s -m "Update to $TARGET_VERSION $BUILD"
fi
