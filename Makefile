INSTALLER_RPM ?= https://dl.fedoraproject.org/pub/fedora/linux/updates/40/Everything/aarch64/Packages/a/asahi-installer-package-0.7.8-2.fc40.aarch64.rpm

all: deploy

installer:
	mkdir installer
	curl -s "$(INSTALLER_RPM)" | rpm2cpio - | cpio -id -D installer
	mv installer/usr/lib64/asahi-installer/releases/* installer/
	rm -r installer/usr

deploy: installer
	mkdir build
	cp -pr installer build/
	cp -p install dev builds builds-dev installer_data_stable.json *.html *.png *.svg build/

clean:
	rm -rf build installer

.PHONY: deploy installer clean
